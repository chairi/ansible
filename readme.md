



# TP1


```bash
cd tp1
```


#### to install apache server on ubuntu


```bash
ansible-playbook playbooks/playbook1.yaml -K
```

#### to install apache server on centos


```bash
ansible-playbook playbooks/playbook1.yaml -K
```

#### to install apache server on both ubuntu and centos


```bash
ansible-playbook playbooks/playbook3.yaml -K
```



#### lamp stack on ubuntu


```bash
ansible-playbook playbooks/lamp.yaml -K
```

